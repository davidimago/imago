<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class siswaController extends Controller
{
    public function index()
    {
        $siswa = Siswa::all()->toArray();
		return view('siswa.index', compact('siswa'));
    }

    public function create()
    {
        return view('siswa.create');
    }

    public function store(Request $request)
    {
		$siswa = $this->validate(request(), [
		'nama' => 'required',
		'nohp' => 'required|numeric',
		'email' => 'required'
		]);
		siswa::create($siswa);
		return back()->with('success', 'Data siswa telah ditambahkan');;
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $siswa = siswa::find($id);
		return view('siswa.edit',compact('siswa','id'));
    }

    public function update(Request $request, $id)
    {
        $siswa = siswa::find($id);
		$this->validate(request(), [
				'nama' => 'required',
				'nohp' => 'required|numeric',
				'email' => 'required']);
		$siswa->nama = $request->get('nama');
		$siswa->nohp = $request->get('nohp');
		$siswa->email = $request->get('email');
		$siswa->save();
		return redirect('siswa')->with('success','Data siswa telah diperbarui');
    }

    public function destroy($id)
    {
        $siswa = siswa::find($id);
		$siswa->delete();
		return redirect('siswa')->with('success','Data siswa telah dihapus');
    }
}
