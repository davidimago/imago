<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiswasTable extends Migration
{
    public function up()
    {
        Schema::create('siswas', function (Blueprint $table) {
            $table->increments('id');
			$table->string('nama');
			$table->integer('nohp');
			$table->string('email');
        });
    }

    public function down()
    {
        Schema::dropIfExists('siswas');
    }
}
