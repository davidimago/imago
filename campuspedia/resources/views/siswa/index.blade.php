<!-- index.blade.php -->
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Index Data Siswa</title>
<link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>
<div class="container">
<br />
@if (\Session::has('success'))
<div class="alert alert-success">
<p>{{ \Session::get('success') }}</p>
</div><br />
@endif
<table class="table table-striped">
<thead>
<tr>
<th>ID</th>
<th>Nama</th>
<th>No HP</th>
<th>Email</th>
<th colspan="2">Action</th>
</tr>
</thead>
<tbody>
@foreach($siswa as $siswa)
<tr>
<td>{{$siswa['id']}}</td>
<td>{{$siswa['nama']}}</td>
<td>{{$siswa['nohp']}}</td>
<td>{{$siswa['email']}}</td>
<td><a href="{{action('siswaController@show', $siswa['id'])}}"
class="btn btn-success">Detail</a></td>
<td><a href="{{action('siswaController@edit', $siswa['id'])}}"
class="btn btn-warning">Ubah</a></td>
<td>
<form action="{{action('siswaController@destroy',
$siswa['id'])}}" method="post">
{{csrf_field()}}
<input name="_method" type="hidden" value="DELETE">
<button class="btn btn-danger" type="submit">Hapus</button>
</form>
</td>
</tr>
@endforeach
<tr>
	<td><a href="{{action('siswaController@create', $siswa['id'])}}"
		   class="btn btn-primary">Tambah Data</a></td>
</tr>
</tbody>
</table>
</div>
</body>
</html>