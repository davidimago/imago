<!-- edit.blade.php -->
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Edit Data Siswa</title>
<link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>
<div class="container">
<h2>Perubahan Data</h2><br />
@if ($errors->any())
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div><br />
@endif
<form method="post" action="{{action('siswaController@update', $id)}}">
{{csrf_field()}}
<input name="_method" type="hidden" value="PATCH">
	<div class="row">
		<div class="col-md-4"></div>
			<div class="form-group col-md-4">
			<label for="nama">Nama:</label>
			<input type="text" class="form-control" name="name" value="{{$siswa->nama}}">
			</div>
	</div>
	<div class="row">
		<div class="col-md-4"></div>
			<div class="form-group col-md-4">
			<label for="nohp">No HP:</label>
			<input type="text" class="form-control" name="nohp" value="{{$siswa->nohp}}">
			</div>
	</div>
	<div class="row">
		<div class="col-md-4"></div>
			<div class="form-group col-md-4">
			<label for="email">Email:</label>
			<input type="text" class="form-control" name="email" value="{{$siswa->email}}">
			</div>
	</div>
<div class="row">
<div class="col-md-4"></div>
<div class="form-group col-md-4">
<button type="submit" class="btn btn-success" style="marginleft:
38px">Update Data</button>
</div>
</div>
</form>
</div>
</body>
</html>